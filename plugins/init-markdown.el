;; init-markdown.el
;; ------------------------------------
;; markdown-mode (MELPA)

(use-package markdown-mode :ensure t)
(autoload 'markdown-mode "markdown-mode"
  "Major mode for editing Markdown files" t)

(provide 'init-markdown)

